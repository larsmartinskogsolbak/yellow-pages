﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Name_Search
{
    class Person
    {
        private string firstName;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public Boolean employed = false; 

        public Person()
        {

        }
        public Person(string firstName, string lastName, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public void isEmployed()
        {
            employed = true; 
        }
    }
}
