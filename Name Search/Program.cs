﻿using System;
using System.Collections.Generic;

namespace Name_Search
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] persons =
            {
                new Person("Lars-Martin", "Solbak", "12345678"),
                new Person("Steve", "Wells", "87654321"),
                new Person("Beatriz", "Cotton","11223344"),
                new Person("Marni", "Malone", "81726354"),
                new Person("Asher", "Xiong", "44321789")
            };
            persons[0].isEmployed();
            persons[1].isEmployed();

            string query = InputSearch();
            SearchNames(persons, query);

        }
        private static string InputSearch()
        {
            Console.WriteLine("Search for a name (first or last name)");
            return Console.ReadLine();

        }

        private static void SearchNames(object[] persons, string query)
        {

            foreach (Person person in persons)
            {
                if ((person.FirstName.Contains(query) || (person.LastName.Contains(query))))
                {
                    if (person.employed == true)
                    {
                        Console.WriteLine(person.FirstName + " " + person.LastName);
                        Console.WriteLine(person.PhoneNumber);
                    }
                    else if (person.employed == false)
                    {
                        Console.WriteLine("You can only search for current employees.");
                    }
                }
            }
        }
    }
}
